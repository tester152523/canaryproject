#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define LISTENING_PORT 8000

static void serve_new_connection(FILE *fp) {
  char method[10], uri[100], protocol[20];
  fscanf(fp, "%s %s %s", method, uri, protocol);
  fprintf(fp, "HTTP/1.0 200 OK\r\n\r\nURI: [%s]\n", uri);
}

int main() {
  int sock, on = 1;

  struct sockaddr_in local_sa = {};
  local_sa.sin_family = AF_INET;
  local_sa.sin_port = htons(LISTENING_PORT);

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket");
  } else if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) != 0) {
    perror("setsockopt");
  } else if (bind(sock, (struct sockaddr *) &local_sa, sizeof(local_sa)) != 0) {
    perror("bind");
  } else if (listen(sock, 10) != 0) {
    perror("listen");
  } else {
    for (;;) {
      struct sockaddr_in remote_sa = {};
      socklen_t slen = sizeof(remote_sa);
      int new_sock = accept(sock, (struct sockaddr *) &remote_sa, &slen);
      if (new_sock != -1 && fork() == 0) {
        serve_new_connection(fdopen(new_sock, "rb+"));
        exit(0);
      }
      close(new_sock);  // Parent closes new socket
    }
  }

  return 0;
}
